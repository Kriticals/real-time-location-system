#pragma once

#include "SensorData.h"
#include <rxcpp/rx.hpp>

template <typename T>

class ISensor {
public:
  virtual ~ISensor() = default;

  /**
   * @brief Provides an observable stream of sensor positions.
   */ 
  virtual rxcpp::observable<SensorData<T>> GetDataStream() = 0;
};