#include <memory>
#include "IPositionProvider.h"
#include "ZeroMQPublisher.h"

/**
 * @class PublishSensorDataTask
 * @brief Manages the task of publishing sensor data.
 *
 * PublishSensorDataTask is responsible for taking sensor data from a position provider
 * and publishing it using a specified data publisher.
 */
class PublishSensorDataTask
{
public:
  PublishSensorDataTask(std::shared_ptr<IPositionProvider> positionProvider, std::shared_ptr<ZeroMQPublisher> zmqPublisher) : positionProvider(positionProvider), zmqPublisher(zmqPublisher){}

  rxcpp::composite_subscription Execute()
  {
    auto publisherThread = rxcpp::observe_on_new_thread();

    return positionProvider->GetPositionStream()
      .subscribe_on(publisherThread)
      .subscribe(
        [this](const SensorData<Eigen::Vector3f> &sensorData){ 
          zmqPublisher->Publish(sensorData);
        }
      );
  }

private:
  std::shared_ptr<IPositionProvider> positionProvider;
  std::shared_ptr<ZeroMQPublisher> zmqPublisher;
};