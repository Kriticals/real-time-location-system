#pragma once

#include "IPositionProvider.h"
#include "ISensor.h"
#include <rxcpp/rx.hpp>
#include <vector>
#include <Eigen/Core>

/**
 * @class MockPositionGenerator
 * @brief Generates mock position data for a set of sensors.
 *
 * This class is responsible for creating and managing a collection of
 * MockPositionSensors, each simulating position data within specified spatial boundaries.
 */
class MockPositionGenerator : public IPositionProvider {
private:
  std::vector<std::shared_ptr<ISensor<Eigen::Vector3f>>> _sensors;

public:
  /**
   * @brief Constructor for MockPositionGenerator.
   * @param sensorCount Number of sensors to simulate.
   * @param frequencyHz Frequency of data generation in Hertz.
   * @param minPosition Minimum spatial boundary for position data.
   * @param maxPosition Maximum spatial boundary for position data.
   */
  MockPositionGenerator(int sensorCount, float frequencyHz, Eigen::Vector3f minPosition, Eigen::Vector3f maxPosition);
  rxcpp::observable<SensorData<Eigen::Vector3f>> GetPositionStream() override;
};
