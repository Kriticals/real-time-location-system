#pragma once

#include <cstdint>

template <typename T>

struct SensorData {
  uint64_t sensorId;
  uint64_t timestamp_usec;
  T value;
};