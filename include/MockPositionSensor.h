#pragma once

#include <cstdint>
#include "ISensor.h"
#include <rxcpp/rx.hpp>
#include <Eigen/Core>
#include <chrono>

/**
 * @class MockPositionSensor
 * @brief Simulates a position sensor generating mock data.
 *
 * This class represents an individual position sensor, capable of generating
 * mock position and velocity data. It uses configurable parameters to simulate
 * realistic sensor behavior within defined spatial and velocity boundaries.
 */
class MockPositionSensor : public ISensor<Eigen::Vector3f> {
private:
  const uint64_t _sensorId;
  float _frequencyHz;
  Eigen::Vector3f _minPosition;
  Eigen::Vector3f _maxPosition;
  Eigen::Vector3f _lastPosition;
  Eigen::Vector3f _lastVelocity;
  uint64_t _lastTimestamp;
  std::unique_ptr<rxcpp::observable<SensorData<Eigen::Vector3f>>> _positionObservable;

  void StartEmitting();
  void Update(uint64_t timestamp_usec);

  /**
   * @brief Generates a random position within specified boundaries.
   * @param min The minimum boundary vector for position.
   * @param max The maximum boundary vector for position.
   * @return A randomly generated position vector within the specified boundaries.
   */
  Eigen::Vector3f GenerateRandomPosition(Eigen::Vector3f min, Eigen::Vector3f max);

  /**
   * @brief Generates a random velocity within specified speed limits.
   * @param minSpeedMetersPerSecond The minimum speed for the velocity vector, in meters per second.
   * @param maxSpeedMetersPerSecond The maximum speed for the velocity vector, in meters per second.
   * @return A randomly generated velocity vector within the specified speed limits.
   */
  Eigen::Vector3f GenerateRandomVelocity(float minSpeedMetersPerSecond, float maxSpeedMetersPerSecond);

  /**
   * @brief Generates a random 3d vector with length between min and max limits.
   * @param min The minimum vector length.
   * @param max The maximum vector length.
   */
  Eigen::Vector3f GenerateNoise(float min, float max);

  /**
   * @brief Gets the current timestamp in microseconds.
   */
  uint64_t GetTimestamp();

public:
  /**
   * @brief Constructor for MockPositionSensor.
   * @param sensorId The unique identifier for the sensor.
   * @param frequencyHz The frequency at which the sensor generates data, in Hertz.
   * @param minPosition The minimum boundary for the sensor's position data.
   * @param maxPosition The maximum boundary for the sensor's position data.
   */
  MockPositionSensor(long sensorId, float frequencyHz, Eigen::Vector3f minPosition, Eigen::Vector3f maxPosition);
  rxcpp::observable<SensorData<Eigen::Vector3f>> GetDataStream();
};