#pragma once

#include "SensorData.h"
#include <Eigen/Core>
#include <rxcpp/rx.hpp>
#include <vector>

class IPositionProvider {
public:
  virtual ~IPositionProvider() = default;

  // Provide an observable stream of positions
  virtual rxcpp::observable<SensorData<Eigen::Vector3f>> GetPositionStream() = 0;
};