#pragma once

#include <fstream>
#include <nlohmann/json.hpp>
#include <Eigen/Core>
#include <string>

struct ServerConfiguration
{
  int zmqPublisherPort;
  int sensorCount;
  float sensorFrequencyHz;
  Eigen::Vector3f minPosition;
  Eigen::Vector3f maxPosition;

  /**
   * Loads configuration from a JSON file.
   * @param filename Path to the JSON configuration file.
   * @return A ServerConfiguration object with loaded settings.
   */
  static ServerConfiguration Load(const std::string &filename)
  {
    std::ifstream configFile(filename);
    if (!configFile.is_open())
    {
      throw std::runtime_error("Unable to open configuration file: " + filename);
    }

    nlohmann::json config;
    configFile >> config;

    ServerConfiguration serverConfig;
    serverConfig.zmqPublisherPort = config["zmqPublisherPort"];
    serverConfig.sensorCount = config["sensorCount"];
    serverConfig.sensorFrequencyHz = config["sensorFrequencyHz"];

    std::vector<float> minPos = config["minPosition"].get<std::vector<float>>();
    serverConfig.minPosition = Eigen::Vector3f(minPos[0], minPos[1], minPos[2]);

    std::vector<float> maxPos = config["maxPosition"].get<std::vector<float>>();
    serverConfig.maxPosition = Eigen::Vector3f(maxPos[0], maxPos[1], maxPos[2]);

    return serverConfig;
  }
};