#pragma once

#include "SensorData.h"
#include <Eigen/Core>
#include <zmq.hpp>

/**
 * @class ZeroMQPublisher
 * @brief Publishes sensor data using ZeroMQ.
 *
 * This class encapsulates the functionality required to serialize sensor data
 * and publish it over a ZeroMQ PUB socket.
 */
class ZeroMQPublisher {
private:
  zmq::context_t context;
  zmq::socket_t socket;

public:
  /**
   * @brief Constructor for ZeroMQPublisher.
   * @param port Port number to bind the ZeroMQ publisher socket.
   */
  ZeroMQPublisher(int port);
  ~ZeroMQPublisher();

  /**
   * @brief Publishes the provided sensor data.
   * @param sensorData The sensor data to be published.
   */
  void Publish(const SensorData<Eigen::Vector3f>& sensorData);
};
