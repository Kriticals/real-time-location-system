#include <iostream>
#include <fstream>
#include <vector>
#include <Eigen/Dense>
#include <nlohmann/json.hpp>
#include "ServerConfiguration.h"
#include "MockPositionGenerator.h"
#include "ZeroMQPublisher.h"
#include "PublishSensorDataTask.h"

int main(int, char**){
  std::shared_ptr<ServerConfiguration> config;
  try{
    config = std::make_shared<ServerConfiguration>(ServerConfiguration::Load("serverconf.json"));
  }catch(...){
    std::cerr << "Exception while reading serverconf.json file." << std::endl;
    return 1;
  }
  

  std::shared_ptr<ZeroMQPublisher> zmqPublisher;
  try{
    zmqPublisher = std::make_shared<ZeroMQPublisher>(config->zmqPublisherPort);
  }catch(zmq::error_t error){
    std::cerr << "Exception while initializing ZeroMQPublisher." << std::endl;
    std::cerr << std::string(error.what()) << std::endl;
    return 1;
  }
  
  std::shared_ptr<IPositionProvider> positionProvider = 
    std::make_shared<MockPositionGenerator>(
      config->sensorCount, 
      config->sensorFrequencyHz, 
      config->minPosition, 
      config->maxPosition
    );

  PublishSensorDataTask publishSensorDataTask(positionProvider, zmqPublisher);
  auto publishSubscription = publishSensorDataTask.Execute();
  
  std::cout << "Press Enter to exit..." << std::endl;
  std::cin.get();

  publishSubscription.unsubscribe();

  return 0;
}