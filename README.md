# Real-time Location System

Demo project using ZeroMQ and protobuf for publishing simulated positional data in a pub-sub pattern.

## Dependencies

This project uses vcpkg to manage dependencies.
Please install vcpkg as described here: [https://vcpkg.io/en/getting-started.html](https://vcpkg.io/en/getting-started.html)

In order for cmake to find installed dependencies from vcpkg, create a CMakePresets.json with the following contents, replacing the "***PATH_TO_VCPKG***" to your local vcpkg installation:
```
{
  "version": 3,
  "configurePresets": [
      {
          "name": "default",
          "binaryDir": "${sourceDir}/build",
          "cacheVariables": {
              "CMAKE_TOOLCHAIN_FILE": "***PATH_TO_VCPKG***/vcpkg/scripts/buildsystems/vcpkg.cmake"
          }
      }
  ]
}
```

For more information on the integration of vcpkg and cmake, check out [the Microsoft Guide on vcpkg in CMake projects](https://learn.microsoft.com/en-us/vcpkg/users/buildsystems/cmake-integration).