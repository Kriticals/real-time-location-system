#include "ZeroMQPublisher.h"
#include <string>
#include <iostream>
#include <position.pb.h>

ZeroMQPublisher::ZeroMQPublisher(int port) : context(1), socket(context, ZMQ_PUB) {
  // Bind the publisher socket to a TCP port
  socket.bind("tcp://*:" + std::to_string(port)); // Replace with the appropriate address and port
}

ZeroMQPublisher::~ZeroMQPublisher() {
  socket.close();
}

void ZeroMQPublisher::Publish(const SensorData<Eigen::Vector3f>& sensorData) {
  rtls::proto::Position positionMsg;
  positionMsg.set_sensorid(sensorData.sensorId);
  positionMsg.set_timestamp_usec(sensorData.timestamp_usec);

  auto data3d = positionMsg.mutable_position();
  data3d->set_x(sensorData.value.x());
  data3d->set_y(sensorData.value.y());
  data3d->set_z(sensorData.value.z());

  std::string positionMsgSerialized;
  if (!positionMsg.SerializeToString(&positionMsgSerialized)) {
      std::cerr << "Failed to serialize." << std::endl;
      return;
  }

  zmq::message_t zmqMessage(positionMsgSerialized.data(), positionMsgSerialized.size());
  try {
    socket.send(zmqMessage, zmq::send_flags::none);
  } catch (const zmq::error_t& e) {
    std::cerr << "Sending ZMQ Message failed: " << e.what() << std::endl;
    // TODO: Possibly cache failed messages for resending them.
  }
}
