#include "MockPositionSensor.h"

MockPositionSensor::MockPositionSensor(long sensorId, float frequencyHz, Eigen::Vector3f minPosition, Eigen::Vector3f maxPosition)
    : _sensorId(sensorId),
      _frequencyHz(frequencyHz),
      _minPosition(minPosition),
      _maxPosition(maxPosition)
{
}

Eigen::Vector3f MockPositionSensor::GenerateRandomPosition(Eigen::Vector3f min, Eigen::Vector3f max)
{
  // Generate a random vector where each component is in [-1, 1]
  Eigen::Vector3f randomVec = Eigen::Vector3f::Random();
  // Scale/shift all components to be within [0, 1]
  randomVec += Eigen::Vector3f(1.0f, 1.0f, 1.0f);
  randomVec *= 0.5f;
  Eigen::Vector3f scale = (max - min);
  Eigen::Vector3f shift = min;
  randomVec = randomVec.cwiseProduct(scale) + shift;

  return randomVec;
}

Eigen::Vector3f MockPositionSensor::GenerateRandomVelocity(float minSpeedMetersPerSecond, float maxSpeedMetersPerSecond)
{
  // Generate a random vector where each component is in [-1, 1]
  Eigen::Vector3f randomVec = Eigen::Vector3f::Random();
  // Scale to z component to [0, 1] range for speed value generation.
  auto randVal = (randomVec.z() + 1) / 2; 
  auto targetSpeed = randVal * (maxSpeedMetersPerSecond - minSpeedMetersPerSecond) + minSpeedMetersPerSecond;
  randomVec.z() = 0;
  return randomVec.normalized() * targetSpeed;
}

Eigen::Vector3f MockPositionSensor::GenerateNoise(float minLength, float maxLength)
{
  // Generate a random vector where each component is in [-1, 1]
  Eigen::Vector3f randomVec = Eigen::Vector3f::Random();
  auto targetNoiseLength = minLength + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(maxLength-minLength)));
  // Scale to random noise length between minLength and maxLength.
  return randomVec.normalized() * targetNoiseLength;
}

uint64_t MockPositionSensor::GetTimestamp()
{
  auto now = std::chrono::high_resolution_clock::now();
  uint64_t microseconds = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch()).count();
  return microseconds;
}

rxcpp::observable<SensorData<Eigen::Vector3f>> MockPositionSensor::GetDataStream()
{
  if(!_positionObservable){
    // Observable not yet created, i.e. this is the first time an observable is requested.
    _lastPosition = GenerateRandomPosition(_minPosition, _maxPosition);
    _lastVelocity = GenerateRandomVelocity(4.0f, 5.5f);

    auto sensorId = _sensorId;
    auto usecStep = static_cast<long>(1.0 / _frequencyHz * 1000000);
    _positionObservable = std::make_unique<rxcpp::observable<SensorData<Eigen::Vector3f>>>(
      rxcpp::observable<>::interval(std::chrono::microseconds(usecStep))
      .map(
        [this, usecStep](long count){
          Update(count * usecStep);
          auto data = SensorData<Eigen::Vector3f>{
            .sensorId = _sensorId,
            .timestamp_usec = GetTimestamp(),
            .value = _lastPosition + GenerateNoise(0.0f, 0.3f)
          };
          return data;
        }
      )
    );
  }

  return *_positionObservable;
}

void MockPositionSensor::Update(uint64_t timestamp_usec)
{
  if(_lastTimestamp == 0){
    _lastTimestamp = timestamp_usec;
  }
  // Time difference in seconds
  auto deltaTime = (timestamp_usec - _lastTimestamp) / 1000000.0f;
  Eigen::Vector3f newPosition = _lastPosition + _lastVelocity * deltaTime;
  Eigen::Vector3f newVelocity = _lastVelocity;

  // Ensure to stay within bounds, by bouncing off bounds.
  if(newPosition.x() < _minPosition.x()){
    newPosition.x() = _minPosition.x();
    newVelocity.x() *= -1.0f;
  } 
  else if(newPosition.x() > _maxPosition.x()){
    newPosition.x() = _maxPosition.x();
    newVelocity.x() *= -1.0f;
  }

  if(newPosition.y() < _minPosition.y()){
    newPosition.y() = _minPosition.y();
    newVelocity.y() *= -1.0f;
  } 
  else if(newPosition.y() > _maxPosition.y()){
    newPosition.y() = _maxPosition.y();
    newVelocity.y() *= -1.0f;
  }

  _lastPosition = newPosition;
  _lastVelocity = newVelocity;
  _lastTimestamp = timestamp_usec;
  auto data = SensorData<Eigen::Vector3f>{
    .sensorId = _sensorId,
    .timestamp_usec = timestamp_usec,
    .value = newPosition
  };
}


