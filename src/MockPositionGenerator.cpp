#include "MockPositionGenerator.h"
#include "MockPositionSensor.h"
#include "SensorData.h"
#include <Eigen/Core>
#include <rxcpp/rx.hpp>
using namespace rxcpp::operators;

MockPositionGenerator::MockPositionGenerator(int sensorCount, float frequencyHz, Eigen::Vector3f minPosition, Eigen::Vector3f maxPosition)
{
    _sensors.reserve(sensorCount);
    for (int sensorId = 0; sensorId < sensorCount; sensorId++)
    {
        auto sensor = std::make_shared<MockPositionSensor>(sensorId, frequencyHz, minPosition, maxPosition);
        _sensors.push_back(sensor);
    }
}

rxcpp::observable<SensorData<Eigen::Vector3f>> MockPositionGenerator::GetPositionStream()
{
  auto sensor_data_stream = rxcpp::observable<>::iterate(_sensors) |
    flat_map([](std::shared_ptr<ISensor<Eigen::Vector3f>> sensor){
        return sensor->GetDataStream();
    });

    return sensor_data_stream;
}
