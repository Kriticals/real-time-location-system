#include <zmq.hpp>
#include <iostream>
#include <string>
#include "position.pb.h"

int main() {
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_SUB);

    // Connect to the publisher's address
    std::string address = "tcp://localhost:5555";
    socket.connect(address);
    
    // Subscribe to all messages from the publisher
    socket.set(zmq::sockopt::subscribe, "");

    while (true) {
        zmq::message_t message;
        auto result = socket.recv(message, zmq::recv_flags::none);
        if(!result){
          std::cerr << "Failed to receive message.";
          break;
        }

        rtls::proto::Position positionMsg;
        auto msgData = std::string(message.data<char>(), message.size());
        if(!positionMsg.ParseFromString(msgData)){
          std::cerr << "Could not parse protocol buffer." << std::endl;
        } else {
          std::cout << "Received Msg: SensorId " << positionMsg.sensorid() << ", Timestamp " << positionMsg.timestamp_usec() << ", Position (" << positionMsg.position().x() << ", " << positionMsg.position().y() << ", " << positionMsg.position().z() << ")" << std::endl;
        }
    }

    return 0;
}